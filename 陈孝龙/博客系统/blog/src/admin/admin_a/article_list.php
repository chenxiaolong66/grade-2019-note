<?php
date_default_timezone_set("PRC");

$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn, "root","123456");
$db->exec("set names utf8mb4");
$sql="SELECT * FROM article order by article_id desc";
$result=$db->query($sql);
$articleList=$result->fetchAll(PDO::FETCH_ASSOC);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link rel="stylesheet" href="Untitled-1.css" />
</head>

<body>
<div id="container">
<div id="top">
<h1>博客管理系统</h1>
<div id="login">欢迎你：admin<a href="#">退出登录</a>
</div>
</div>

<div id="left">
<ul>
<li><a href="#">分类管理</li>
<li><a href="#">新闻管理</li>

</ul>
</div>

<div id="right">
<div id="a">
<a href="#">首页</a>>
<a href="#">新闻管理</a>>
<a href="#">新闻列表</a>
</div>
<div id="bb">
<div id="b">

</div>
<div id="aa">
<a href="article_add.php" >增加新闻</a>
</div>
</div>

<table border="" cellpadding="" cellspacing="">
<tr>
<th></th>
<th>Id</th>
<th>标题</th>
<th>分类</th>
    <th>简介</th>
<th>发表时间</th>
<th>修改时间</th>
<th>操作</th>

</tr>
    <?php foreach ($articleList as $row): ?>
<tr>
<td><input type="checkbox" /></td>
<td><?php echo $row['article_id']; ?></td>
    <td><?php echo $row['article_title']; ?></td>
<td><?php echo $row['category_id'];
$sql="select * from category where  category_id='{$row['category_id']}'";
    $result=$db->query($sql);
$category=$result->fetch(PDO::FETCH_ASSOC);
echo  $category['category_name'];
?></td>

<td><?php echo $row['intro']; ?></td>
<td><?php echo date("Y-m-d H:i:s",$row['add_time']); ?></td>
<td><?php echo date("Y-m-d H:i:s",$row['update_time']); ?></td>
<td><a href="article_edit.php? article_id=<?php  echo  $row['article_id']; ?>">编辑</a>
    <a href="article_delete.php? article_id=<?php echo $row['article_id']; ?>">删除</a>
</td>
</tr>
    <?php endforeach; ?>
</table>

</div>

</div>
</body>
</html>
