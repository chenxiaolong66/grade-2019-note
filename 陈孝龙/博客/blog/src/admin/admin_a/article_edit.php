<?php
date_default_timezone_set("PRC");

$articleId=$_GET['article_id'];

$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn, "root","123456");
$db->exec("set names utf8mb4");

$sql="SELECT * FROM category order by category_id desc";
$result=$db->query($sql);
$categoryList=$result->fetchAll(PDO::FETCH_ASSOC);

$sql="SELECT * FROM article where article_id='$articleId'";
$result=$db->query($sql);
$article=$result->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link rel="stylesheet" type="text/css" href="Untitled-2.css" />
</head>

<body>
<div id="container">
    <div id="top">
        <h1>博客管理系统</h1>
        <div id="login">欢迎你：admin<a href="#">退出登录</a>
        </div>
    </div>

    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</li>
            <li><a href="#">新闻管理</li>
            <li><a href="#">管理员</li>
        </ul>
    </div>

    <div id="right">
        <div id="a">
            <a href="#">首页</a>>
            <a href="#">文章管理</a>>
            <a href="#">文章分类</a>
        </div>
        <form action="article_edit_save.php" method="post">

            <table border="" cellpadding="" cellspacing="">

                <tr>
                    <td>文章标题</td>
                    <td><input type="text"  name="article_title" value="<?php echo $article['article_title']; ?>" /></td>
                </tr>
                <tr>
                    <td>所属分类</td>
                    <td>
                        <select name="category_id">
                            <?php foreach ($categoryList as $row):?>
                                <option value="<?php echo  $row['category_id'];?>">
                                    <?php echo $row['category_name'] ;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>文章简介</td>
                    <td><textarea name="intro" <?php echo $article['intro'];?>></textarea></td>
                </tr>
                <tr>

                <tr>
                    <td>文章内容</td>
                    <td><textarea name="content" <?php echo $article['content'];?>></textarea></td>
                </tr>
                <td></td>
                <td><input type="submit" value="提交" />
                    <input type="submit" value="重置" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>


</body>
</html>
