<?php
session_start();
$_image=imagecreate(58,25);
$white=imagecolorallocate($_image,255,255,255);
$verifyCode='';
for ($i=1;$i<=4;$i++){
    $str=mt_rand(0,9);
    $font=4;
    $x=2+($i-1)*15;
    $y=5;
    $color=imagecolorallocate($_image,255,192,203);
    imagestring($_image,$font,$x,$y,$str,$color);
    $verifyCode .=$str;
}
$_SESSION['verify_code']=$verifyCode;
header("Content-type:image/png");
imagepng($_image);
imagedestroy($_image);