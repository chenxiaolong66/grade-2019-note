<?php
$adminEmail=$_POST['admin_email'];
$adminPassword=$_POST['admin_password'];
$verifyCode=$_POST['verify_code'];
$rememberMe=$_POST['remember_me'] ??'';

session_start();
if ($verifyCode!=$_SESSION['verify_code']){
    echo "验证码错误.<a href='login.php'>返回登录界面</a>";
    exit();
}

date_default_timezone_set("PRC");

$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn, "root","123456");
$db->exec("set names utf8mb4");

$sql="SELECT * FROM admin where admin_email='{$adminEmail}'";

$result=$db->query($sql);
$admin=$result->fetch(PDO::FETCH_ASSOC);


if (!$admin){
    echo "账号或者密码不正确,请重新输入。 <a href='login.php'>返回登录界面</a>";
    exit();
}

if ($admin['admin_password']==$adminPassword){

    $_SESSION['admin_email']=$adminEmail;
    $_SESSION['admin_name']=$admin['admin_name'];
if ($rememberMe){
    setcookie('PHPSESSID',session_id(),time()+20);
}

    echo "登录成功。<a href='category_list.php'>前往列表</a>";
    exit();
}else{
    echo "账号或者密码不正确,请重新输入。<a href='login.php'></a>";
    exit();
}