<?php
session_start();
if (empty($_SESSION['admin_email'])){
    echo '尚未登录，请重新登录。<a href="login.php">登录</a>';
    exit();
}

date_default_timezone_set("PRC");

$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn, "root","123456");
$db->exec("set names utf8mb4");
$sql="SELECT * FROM admin order by admin_id desc";
$result=$db->query($sql);
$adminList=$result->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link rel="stylesheet" type="text/css" href="Untitled-1.css" />
</head>
<body>

<div id="container">
    <div id="top">
        <h1>博客管理系统</h1>
        <div id="login">欢迎你：<?php  echo $_SESSION['admin_name'] ; ?>
            <a href="#">退出登录</a>
        </div>
    </div>

    <div id="left">
        <ul>
            <li><a href="category_add.php">分类管理</li>
            <li><a href="article_list.php">新闻管理</li>
            <li><a href="#">管理员</li>
        </ul>
    </div>

    <div id="right">

        <div id="bb">
            <div id="b">
                <button>全选</button>
                <a href="#">删除选中任务</a>
            </div>
            <div id="aa">
                <a href="admin_add.php" >增加管理员</a>
            </div>
        </div>

        <table border="" cellpadding="" cellspacing="">
            <tr>
                <th></th>
                <th>管理员Id</th>
                <th>姓名</th>
                <th>邮箱</th>
                <th>增加时间</th>
                <th>修改时间</th>
                <th>操作</th>
            </tr>
            <?php foreach ($adminList as $row): ?>
                <tr>
                    <th><input type="checkbox" /></th>
                    <td><?php echo $row['admin_id'];?></td>
                    <td><?php echo  $row['admin_name'];?></td>
                    <td><?php echo  $row['admin_email'];?></td>
                    <td><?php echo date("Y-m-d H:i:s", $row['add_time']);?></td>
                    <td><?php echo date("Y-m-d H:i:s", $row['update_time']);?></td>
                    <td><a href="admin_edit.php?admin_id=<?php echo $row['admin_id'];?>">编辑</a>
                        <a href="admin_delete.php?admin_id=<?php echo $row['admin_id'];?>">删除</a> </td>
                </tr>
            <?php  endforeach;?>
        </table>

    </div>

</div>
</body>
</html>
