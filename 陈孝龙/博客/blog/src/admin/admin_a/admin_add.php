<?php
session_start();
if (empty($_SESSION['admin_email'])){
    echo '尚未登录，请重新登录。<a href="login.php">登录</a>';
    exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link rel="stylesheet" type="text/css" href="Untitled-2.css" />
</head>

<body>
<div id="container">
    <div id="top">
        <h1>博客管理系统</h1>
        <div id="login">欢迎你：<?php  echo $_SESSION['admin_name'] ; ?>
            <a href="#">退出登录</a>
        </div>
    </div>

    <div id="left">
        <ul>
            <li><a href="#">分类管理</li>
            <li><a href="#">新闻管理</li>
            <li><a href="#">管理员</li>
        </ul>
    </div>

    <div id="right">

        <form action="admin_save_add.php" method="post">
            <table border="" cellpadding="" cellspacing="">
                <tr>
                    <td>管理员名称</td>
                    <td><input type="text" name="admin_name" /></td>
                </tr>

                <tr>
                    <td>管理员邮箱</td>
                    <td><textarea name="admin_email"></textarea></td>
                </tr>
                <tr>
                    <td>管理员密码</td>
                    <td><textarea name="admin_password"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="提交" />
                        <input type="submit" value="重置" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>

</div>
</body>
</html>

