<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <link rel="stylesheet" type="text/css" href="login.css" />
</head>
<body>
<div id="container">
    <div id="login">
        <div id="login-header">
            <p>非常日记后台管理</p>
        </div>

        <div id="login-center">
            <p>请输入用户名和密码</p>
            <form id="login-center-form" action="login_check.php" method="post">
                <div><input type="text" placeholder="邮箱"  class="textInput" name="admin_email" /></div>
                <div><input type="password" placeholder="密码" class=""  name="admin_password"/></div>
                <div><input type="text" placeholder="验证码" id="verify-code" name="verify_code"/>
                    <img src="verify_code.php" id="verify-code"/>
                </div>
                <div class="remember-me-div"><input type="checkbox" /> <span class="remember-me-span">记住我</span></div>
                <input type="submit" value="登录" class="btn" />
            </form>
        </div>

        <div id="login-footer">新员工 ? 创建账号</div>
    </div>
</div>
</body>
</html>
