<?php
declare (strict_types=1);

namespace app\api\controller;



use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;

class Comment
{
    public function add(){
        $params=Request::param();
       $validate= Validate::rule([
            'comment_content'=>'require|min:2|max:500',
            'email'=>'require|min:3|max:100',
            'nickname'=>'require|min:1|max:20',
           'article_id'=>'require|between:1,'  . PHP_INT_MAX ,
        ]);

       if (!$validate->check($params)){
           $date=[
               'status'=>1002,
               'message'=>$validate->getError(),
               'data'=>[]
           ];
           return json($date);

       }
       $insertDate=$params;
       $insertDate['add_time']=$insertDate['update_time']=time();
       $result=CommentModel::create($insertDate);
       if ($result){
           $date=[
               'status'=>0,
                'message'=>'',
               'data'=>[

                   'comment'=>$result->toArray()
               ]
           ];
           return json($date);

       }
        $date=[
            'status'=>1001,
            'message'=>'系统失败，处理器错误',
            'data'=>[

                'comment'=>$result->toArray()
            ]
        ];
        return json($date);
       var_dump($result);
       exit();



    }
    public  function  list(){
    $articleId=Request::param("article_id");
    $validate=Validate::rule([
       'article_id'=>'require',
    ]);
    if (!$validate->check(['article_id'=>$articleId])){
        $data=[
            'status'=>10002,
            'message'=>$validate->getError(),
            'data'=>[]
        ];
        return json($data);
    }
    $result=CommentModel::where("article_id","=",$articleId)->order("add_time","desc")->select();
    $commentList=[];
    foreach ($result as $item){
        $temp=$item->toArray();
        $commentList[]=[
            'comment_id'=>$temp['comment_id'],
            'comment_content'=>$temp['comment_content'],
            'nickname'=>$temp['nickname'],
            'add_time'=>$temp['add_time'],
        ];
    }
    $data=[
        'status'=>0,
        'message'=>'',
        'data'=>[

            'commentList'=>$commentList
        ]
        ];
    return json($data);
    }
}
