<?php
// 这是系统自动生成的middleware定义文件
return [
    \think\middleware\SessionInit::class, //  启用session
    app\admin\middleware\Auth::class // 登录校验中间件
];
