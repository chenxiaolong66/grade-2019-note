<?php
declare (strict_types=1); // php的严格模式，弱类型

namespace app\admin\controller;

use app\model\ArticleModel;
use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

/**
 * Class Article
 * @package app\admin\controller
 */
class Article
{
    // 文章列表
    public function index()
    {
        $articleList = ArticleModel::getList();
        View::assign('articleList', $articleList);
        return View::fetch();
    }

    public function add()
    {
        $categoryList = CategoryModel::getAllPreKey();
        return View::fetch('', ['categoryList' => $categoryList]);
    }

    public function addSave()
    {
        $params = Request::param();
        $validate = Validate::rule([
            'article_title|文章标题' => 'require|min:5|max:50',
            'intro|文章简介' => 'require|min:5|max:100',
            'content|文章内容' => 'require|min:10|max:20000',
            'category_id|文章分类' => 'require|between:1,' . PHP_INT_MAX
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }

        $params['add_time'] = time();
        $params['update                                                                                                                                                                                                                                                                                                                                                                                                                             _time'] = time();
        $result = ArticleModel::create($params);

        return View::fetch('public/tips', [
            'result' => $result,
            'url' => '/index.php?s=admin/article/index'
        ]);
    }

    // 编辑文章
    public function edit()
    {
        $articleId = Request::param("article_id");
        $validate = Validate::rule([
            'article_id|文章id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['article_id' => $articleId])) {
            echo $validate->getError();
            exit();
        }

        $article = ArticleModel::find($articleId);
        if (!$article) {
            echo '文章不存在';
            exit();
        }
        $categoryList = CategoryModel::getAllPreKey();
        return View::fetch('', ['article' => $article, 'categoryList' => $categoryList]);
    }

    // 保存数据
    public function editSave()
    {
        $params = Request::param();
        $validate = Validate::rule([
            'article_id|文章id' => 'require|between:1,' . PHP_INT_MAX,
            'article_title|文章标题' => 'require|min:5|max:50',
            'intro|文章简介' => 'require|min:5|max:100',
            'content|文章内容' => 'require|min:10|max:20000',
            'category_id|文章分类' => 'require|between:1,' . PHP_INT_MAX
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }

        // update
        // 查找出对应的文章信息
        $article = ArticleModel::find($params['article_id']);
        if (!$article) {
            echo '文章不存在';
            exit();
        }
        $article['article_title'] = $params['article_title'];
        $article['intro'] = $params['intro'];
        $article['content'] = $params['content'];
        $article['category_id'] = $params['category_id'];
        $article['update_time'] = time();
        $result = $article->save();

        return View::fetch('public/tips', [
            'result' => $result,
            'url' => '/index.php?s=admin/article/index'
        ]);
    }

    // 文章删除
    public function delete()
    {
        $articleId = Request::param("article_id");
        $validate = Validate::rule([
            'article_id|文章id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['article_id' => $articleId])) {
            echo $validate->getError();
            exit();
        }

        $result = ArticleModel::destroy($articleId);
        return View::fetch('public/tips', ['result' => $result]);
    }
}
