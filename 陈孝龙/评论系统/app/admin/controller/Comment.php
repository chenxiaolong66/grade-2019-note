<?php
declare (strict_types=1);
namespace app\admin\controller;
use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Comment{

public  function index(){
    $status=Request::param("status","1");
    $commentList=CommentModel::where('status','=',$status)->select();
    View::assign('commentList',$commentList);
    View::assign('status',$status);
    return View::fetch();
}
public  function  garbage(){
    $commentId=Request::param("comment_id");
    $validate=Validate::rule([
       'comment_id|分类id'=>'require',
    ]);
    if (!$validate->check(['comment_id'=>$commentId])){
        echo $validate->getError();
        exit();
    }
    $comment=CommentModel::find($commentId);
    $comment['status']=2;
    $result=$comment->save();
    $date=[
        'status'=>$result?0:10001,
        'message'=>$result?'':'修改数据库',
        'data'=>[
            'result'=>$result?true:false
        ]
    ];
    return json($date);

}
public  function resume(){
    $commentId=Request::param("comment_id");
    $validate=Validate::rule([
        'comment_id|分类id'=>'require',
    ]);
    if (!$validate->check(['comment_id'=>$commentId])){
        echo $validate->getError();
        exit();
    }
    $comment=CommentModel::find($commentId);
    $comment['status']=1;
    $result=$comment->save();
    $date=[
        'status'=>$result?0:10001,
        'message'=>$result?'':'修改数据库',
        'data'=>[
            'result'=>$result?true:false
        ]
    ];
    return json($date);
}
    public  function delete(){
        $commentId=Request::param("comment_id");
        $validate=Validate::rule([
            'comment_id|分类id'=>'require',
        ]);
        if (!$validate->check(['comment_id'=>$commentId])){
            echo $validate->getError();
            exit();
        }

        $result=CommentModel::destroy($commentId);
        $date=[
            'status'=>$result?0:10001,
            'message'=>$result?'':'修改数据库',
            'data'=>[
                'result'=>$result?true:false
            ]
        ];
        return json($date);
    }

}