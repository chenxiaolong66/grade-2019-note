CREATE TABLE `article` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(45) NOT NULL COMMENT '文章标题',
  `content` longtext NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `add_time` datetime NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `category` (
  `CategoryId` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `CategoryName` varchar(20) CHARACTER SET ascii NOT NULL COMMENT '分类名称',
  `CategoryDesc` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT '分类描述',
  `UpdateTime` int(11) NOT NULL COMMENT '修改时间',
  `AddTime` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`CategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
