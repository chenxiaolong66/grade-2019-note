1. 完成 git_0010_介绍/01_git概念.md 第1和2题

## 课堂练习
1. 林纳斯·托瓦兹为什么决定自己写一个版本管理软件，说下你的理解。

2. 开源软件和闭源软件是什么？他们之间各有什么优缺点？

开源软件，源代码是大众可访问的，如果需要，程序员可以查看或者修改代码。
优点：免费的，可以在一个相对短的时间内找到漏洞，增加功能或者提升性能。
缺点：用户界面没有闭源的好，遇到麻烦，寻求技术支持可能比较困难。

闭源软件就是公众熟知的专有软件，公众没法接触源代码，所以他们不能看见或者修改源代码。
优点：稳定性好
缺点：需要收费

2. 完成 git_0010_介绍/02_git安装.md 第1题
3. 完成 git_0020_本地基础操作/01_初始化仓库.md 第1和2题
4. 完成 git_0020_本地基础操作/02_增加文件.md 第1题
5. 完成 git_0020_本地基础操作/03_修改文件.md 第1题